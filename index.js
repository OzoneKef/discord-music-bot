const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs');
const https = require('https');
const config = require('./config');
const { exec } = require('./utils');

const state = {
	play: false,
	track: 0,
	tracklist: [],
	ready: false,
}

let VoiceChannel = false;

client.on('error', console.error);
(async () => {
	state.tracklist = ((await exec('ls ./audio')).split('\n').filter(item => item&&item!==''?item:false));
})()

client.on(`message`, async message => {
	if (message.content.match(/^Player control/)) {
		setTimeout(() => {message.react('⏮');}, 0);
		setTimeout(() => {message.react('⏹');}, 500);
		setTimeout(() => {message.react('▶');}, 1000);
		setTimeout(() => {message.react('⏯');}, 1500);

		setTimeout(() => {
			const filter = (reaction, user) => reaction.emoji.name === '▶'
			const collector = message.createReactionCollector(filter, { time: 300000 });
			collector.on('collect', r => {
				state.play = true;
				console.log('play');
				Play();
				try {
					r.remove(r.users.map(u => u.id!==r.message.author.id?u.id:null).filter(item => item)[0]);
				} catch (error) { console.error(error) }
			});
		}, 2000);
		setTimeout(() => {
			const filter = (reaction, user) => reaction.emoji.name === '⏹'
			const collector = message.createReactionCollector(filter, { time: 300000 });
			collector.on('collect', r => {
				state.play = false;
				Stop();
				console.log('stop');
				try {
					r.remove(r.users.map(u => u.id!==r.message.author.id?u.id:null).filter(item => item)[0]);
				} catch (error) { console.error(error) }
			});
		}, 2000);
		setTimeout(() => {
			const filter = (reaction, user) => reaction.emoji.name === '⏮'
			const collector = message.createReactionCollector(filter, { time: 300000 });
			collector.on('collect', r => {
				state.track = state.track - 1;
				if (state.track<0) {
					state.track = state.tracklist.length-1;
				}
				Reset();
				console.log('prev');
				try {
					r.remove(r.users.map(u => u.id!==r.message.author.id?u.id:null).filter(item => item)[0]);
				} catch (error) { console.error(error) }
			});
		}, 2000);
		setTimeout(() => {
			const filter = (reaction, user) => reaction.emoji.name === '⏯'
			const collector = message.createReactionCollector(filter, { time: 300000 });
			collector.on('collect', r => {
				state.track = state.track + 1;
				if (state.track>state.tracklist.length-1) {
					state.track = 0;
				}
				Reset();
				console.log('next');
				try {
					r.remove(r.users.map(u => u.id!==r.message.author.id?u.id:null).filter(item => item)[0]);
				} catch (error) { console.error(error) }
			});
		}, 2000);
	} else if (message.content.match(/^<@643527860333707276>/)) {
		message.channel.send(`Player control | Треков в базе: ${state.tracklist.length}`);
	} else {
		const attachs = [];
		let uploads = false;
		message.attachments.map(item => {
			try {
				attachs.push(new Promise(async (resolve, reject) => {
					const ext = item.filename.match(/\.(.*)$/);
					if (ext && config.Extensions.includes(ext[0])) {
						if (fs.existsSync(`./audio/${item.filename}`)) {
							await exec(`rm -rf ./audio/${item.filename}`)
						}
						const file = fs.createWriteStream(`./audio/${item.filename}`);
						https.get(item.url, (response) => {
							response.pipe(file);
							state.tracklist.push(item.filename);
							uploads = true;
							resolve();
						});
					}
				}));
			} catch (error) {
				console.log(error)
			}
		});
		await Promise.all(attachs)
		if (uploads) message.react('👌');
	}
});

client.on('ready', () => {
	state.ready = true;
	VoiceChannel = client.channels.find(ch => ch.name === config.Channel);
	Stop();
})

client.login(config.DiscordToken);

let dispatcher = false;

const endCallback = end => {
	state.track === state.tracklist.length - 1 ? state.track=0 : state.track=state.track+1
	Play();
}

function Play() {
	if (!state.ready) return state.play = false;
	VoiceChannel = client.channels.find(ch => ch.name === config.Channel);
	VoiceChannel.join()
		.then(connection =>{
			dispatcher = connection.playFile(`./audio/${state.tracklist[state.track]}`, {volume: 0.4, type: 'mp3/opus', highWaterMark: 1 });
			dispatcher.once("end", endCallback);
		})
		.catch(err => console.log(err));
}

function Stop() {
	if (!state.ready) return state.play = false;
	if (dispatcher) dispatcher.removeListener("end", endCallback)
	VoiceChannel.leave();
}

function Reset() {
	Stop();
	Play();
}