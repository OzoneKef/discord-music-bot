const exec = (string) => {
    return new Promise((resolve, reject) => {
        require('child_process').exec(string, (error, stdout, stderr) => {
            if (error) {console.error(error);reject()};
            resolve(stdout);
        });
    })
}

module.exports = { exec }